require "bibliografia/version"

module Bibliografia
  require 'bibliografia/biblio.rb'
  require 'bibliografia/node.rb'
  require 'bibliografia/linkedlist.rb'
end
